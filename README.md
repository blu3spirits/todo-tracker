# TODO-Tracker
This is where my todos will live

### Dev setup
```
python -m venv .
source bin/activate
pip install -r requirements.txt
cd client
npm install
```

### Running
```
# start up a tmux window
./manage.py runserver
cd client
npm start
```
