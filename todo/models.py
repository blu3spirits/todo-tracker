from django.db import models

# Create your models here.

class Todo(models.Model):
    STATUS_CHOICES = [
        ('CMP', 'Complete'),
        ('OPN', 'Open'),
        ('InP', 'In Progress')
    ]
    title = models.CharField(max_length=180)
    description = models.TextField()
    status = models.CharField(max_length=3,
                              choices=STATUS_CHOICES,
                              default='OPN')

    def __str__(self):
        return self.title
