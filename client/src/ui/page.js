import React from 'react'
import {connect} from 'react-redux'
import {
  Card,
  CardContent,
  Grid,
  Paper,
} from '@material-ui/core'

import Todos from './todos';


const cardStyle = {
  fontSize: 40,
  justifyContent: 'center',
  alignContent: 'center',
  textAlign: 'center'
};

class Page extends React.Component {
//   constructor(props) {
//     super(props);
//     this.handleChange = handleChange.bind(this);
//     this.state = {
//       filtered: []
//     }
//   }
//
//   static getDerivedStateFromProps(props, state) {
//     if(state.filtered) {
//       if(state.filtered.length != props.todos.length) {
//         return state;
//       }
//     }
//     state = {
//       filtered: props.todos
//     };
//     return state;
//   }

  render() {
    return (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper>
            <Card>
              <CardContent style={cardStyle}>
                TODO Application
              </CardContent>
            </Card>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper>
            <Card>
              <CardContent style={cardStyle}>
                <Todos />
              </CardContent>
            </Card>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

function mapStateToProps(state) {
  return state
}
const mapDispatchToProps = {
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page)
