import React from 'react'
import {connect} from 'react-redux'
import {getTodos} from '../actions/todos'
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Input
} from '@material-ui/core'

const columns = [
  { id: 'title', label:'Title'},
  { id: 'description', label: 'Desc.'},
  { id: 'status', label: 'status'},
];

function filterTodos(objects, searchKey) {
  let filteredItems = objects;
  filteredItems = filteredItems.filter((item) => {
    let searchSring = JSON.stringify(item);
    return searchSring.toLowerCase().indexOf(
      searchKey) !== -1
  })
  return filteredItems;
}

function handleChange(event: object) {
  let e = event;
  let todosObjects = this.props.todos;
  let data = filterTodos(todosObjects, e.target.value);
  this.setState({
    filtered: data
  });
}


class Todos extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = handleChange.bind(this);
    this.state = {
      filtered: []
    }
  }
  componentDidMount() {
    this.props.getTodos()
    this.setState({
      filtered: this.props.todos
    });
  }

  static getDerivedStateFromProps(props, state) {
    if(state.filtered) {
      if(state.filtered.length !== props.todos.length) {
        return state;
      }
    }
    state = {
      filtered: props.todos
    };
    return state;
  }

  render() {
    let data = this.props.todos;
    if(this.state.filtered.length > 0) {
      data = this.state.filtered;
    }
    if(!this.props.loading && data.length > 0) {
      return (
        <div className="todos">
          <Input
            onChange={this.handleChange}
            fullWidth={true}
            placeholder="Search..."
            autoFocus={true}
          />
          <Table>
            <TableHead>
              <TableRow>
                {columns.map(column => (
                  <TableCell
                    key={column.id}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map(row => (
                <TableRow key={row.id}>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.description}</TableCell>
                  <TableCell>{row.status}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      )
    } else {
      return (
        <p>Loading...</p>
      )
    }
  }
}

function mapStateToProps(state) {
  return state.todos
}

const mapDispatchToProps = {
  getTodos
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos)
