import {
  GET_TODOS_REQUEST,
  GET_TODOS_SUCCESS,
  GET_TODOS_ERROR
} from '../actions/todos'

const initialState = {
  todos: {},
  loading: false
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TODOS_REQUEST:
      return { ...state, loading: true }
    case GET_TODOS_SUCCESS:
      return {
        ...state,
        todos: action.payload,
        loading: false
      }
    case GET_TODOS_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      }
  default:
    return state
  }
}
