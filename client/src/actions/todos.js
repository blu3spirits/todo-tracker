import {RSAA} from 'redux-api-middleware'

export const GET_TODOS_REQUEST = 'GET_TODOS_REQUEST'
export const GET_TODOS_SUCCESS = 'GET_TODOS_SUCCESS'
export const GET_TODOS_ERROR = 'GET_TODOS_ERROR'

export const getTodos = (options = {}) => dispatch => {
  return dispatch ({
    [RSAA]: {
      endpoint: `http://localhost:8000/api/todos/`,
      method: 'GET',
      types: [
        'GET_TODOS_REQUEST',
        'GET_TODOS_SUCCESS',
        'GET_TODOS_ERROR'
      ]
    }
  })
}
